package com.hatherly.Maze;

/**
 * Object representing individual points - intersection
 * points in the maze
 * @author  Adam Hatherly
 */
public class MazeNodes {
	boolean right = false;
	boolean down = false;
	boolean hasWall = false;
	int downWallNumber = 0;
	int rightWallNumber = 0;
	
	/**
	 * @return  Returns the number of the wall below this node.
	 * @uml.property  name="downWallNumber"
	 */
	public int getDownWallNumber() {
		return downWallNumber;
	}
	/**
	 * @param downWallNumber Sets the number of the wall below this node.
	 * @uml.property  name="downWallNumber"
	 */
	public void setDownWallNumber(int downWallNumber) {
		this.downWallNumber = downWallNumber;
	}
	/**
	 * @return  Returns the rightWallNumber.
	 * @uml.property  name="rightWallNumber"
	 */
	public int getRightWallNumber() {
		return rightWallNumber;
	}
	/**
	 * @param rightWallNumber  The rightWallNumber to set.
	 * @uml.property  name="rightWallNumber"
	 */
	public void setRightWallNumber(int rightWallNumber) {
		this.rightWallNumber = rightWallNumber;
	}
	/**
	 * @return  Returns whether there is a wall below this node.
	 * @uml.property  name="down"
	 */
	public boolean isDown() {
		return down;
	}
	/**
	 * @param down Sets whether there is a wall below this node.
	 * @uml.property  name="down"
	 */
	public void setDown(boolean down) {
		this.down = down;
	}
	/**
	 * @return  Returns whether this node has a wall intersecting it.
	 * @uml.property  name="hasWall"
	 */
	public boolean isHasWall() {
		return hasWall;
	}
	/**
	 * @param hasWall Sets whether there is a wall intersecting this node
	 * @uml.property  name="hasWall"
	 */
	public void setHasWall(boolean hasWall) {
		this.hasWall = hasWall;
	}
	/**
	 * @return Returns whether there is a wall to the right of this node.
	 * @uml.property  name="right"
	 */
	public boolean isRight() {
		return right;
	}
	/**
	 * @param right Sets whether there is a wall to the right of this node.
	 * @uml.property  name="right"
	 */
	public void setRight(boolean right) {
		this.right = right;
	}
}
