package com.hatherly.Maze;
import java.applet.*;
import java.awt.*;
import java.awt.Graphics;

/**
 * This applet generates a random maze and displays it
 * @author  Adam Hatherly
 */
public class MazeApplet extends Applet {
	
	public static final long serialVersionUID = 1;
	private static final boolean DEBUG = false;
	
	private int size_x = 30;
	private int size_y = 30;
	private int wall_length = 10;
	private int offset_x = 5;
	private int offset_y = 5;
	private Dimension d;
    private MazeGenerator gen;
    
    /**
     * Initialises values such as the size and colour of the maze, then
     * calls the MazeGenerator to create the maze
     */
    public void init() {
    	Color c = new Color(215, 244, 214);
    	setBackground( c );
    	
    	d = getSize();
    	int width = d.width - offset_x;
    	int height = d.height - offset_y - 20;
    	size_x = width / wall_length;
    	size_y = height / wall_length;
    	
    	gen = new MazeGenerator(size_x,size_y);
    	gen.makeMaze();
    }

    /**
     * Draws the maze
     */
    public void paint(Graphics g) {
        
    	g.setColor( Color.blue );
    	g.drawString( "Created by Adam Hatherly", d.width-150, d.height-10 );
    	g.setColor( Color.black );
    	
    	for (int x=0; x<size_x; x++) {
    		for (int y=0; y<size_y; y++) {
    			if (DEBUG) {
	    			if (gen.getHasWall(x,y)) {
	    				g.drawOval(offset_x + (x * wall_length) - 2,
	 						   offset_y + (y * wall_length) - 2,
							   4, 4);
	    			}
    			}
    			if (gen.getRightWall(x,y)) {
    				g.drawLine(offset_x + (x * wall_length),
    						   offset_y + (y * wall_length),
    						   offset_x + ((x+1) * wall_length),
    						   offset_y + (y * wall_length));
    				if (DEBUG) {
	    				int num = gen.getRightWallNumber(x,y);
	    				if (num > 0) {
		    				g.drawString(String.valueOf(num),
		    						offset_x + (x * wall_length),
		 						   offset_y + (y * wall_length));
	    				}
    				}
    			}
    			if (gen.getDownWall(x,y)) {
    				g.drawLine(offset_x + (x * wall_length),
    						   offset_y + (y * wall_length),
    						   offset_x + (x * wall_length),
    						   offset_y + ((y+1) * wall_length));
    				if (DEBUG) {
	    				int num = gen.getDownWallNumber(x,y);
	    				if (num > 0) {
		    				g.drawString(String.valueOf(num),
		    						offset_x + (x * wall_length),
		 						   offset_y + (y * wall_length) + 10);
	    				}
    				}
    			}
    		}
    	}
    }
}
