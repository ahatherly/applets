package com.hatherly.Fireworks;

import java.awt.Color;
import java.util.ArrayList;

/**
 * This is the spinning particle object - it is a child of the particle
 * object but adds the ability to "spin" in a circular motion around a
 * moving central point
 * @author Adam Hatherly
 */
public class SpinningParticle extends Particle {

	// Variables to allow for the "spin"
	private final double SpinRate = 0.1;
	private double central_x_pos;
	private double central_y_pos;
	private double angle = 0;
	private int radius;
	
	/**
	 * @param x X position
	 * @param y Y position
	 * @param h Horizontal force
	 * @param u Upwards Force
	 * @param l Lifespan
	 * @param c Colour
	 * @param r Radius of spin
	 */
	public SpinningParticle(int x, int y, double h, double u, int l, Color c, int r) {
		super(x,y,h,u,l,c);
		central_x_pos = x;
		central_y_pos = y;
		radius = r;
	}
	
	/**
	 * This method moves the particle through one iteration of movement, it
	 * takes into effect the force of gravity on the particle. It also spins
	 * it around a circular path as it moves
	 * @param p List of visible particles
	 */
	public boolean Move_Particle(ArrayList p)
	{
		// First, do normal movement of central point
		central_x_pos = central_x_pos + horizontal_force;
		central_y_pos = central_y_pos - upwards_force;
		upwards_force = upwards_force - Gravity;
		
		// Now calculate the actual spinning particle position
		if (angle == 0) {
			x_pos = central_x_pos;
			y_pos = central_y_pos - radius;
		}
		else if (angle < 90) {
			x_pos = central_x_pos + (radius * Math.sin(angle));
			y_pos = central_y_pos - (radius * Math.cos(angle));
		}
		else if (angle == 90) {
			x_pos = central_x_pos + radius;
			y_pos = central_y_pos;
		}
		else if (angle < 180) {
			x_pos = central_x_pos + (radius * Math.cos(angle));
			y_pos = central_y_pos + (radius * Math.sin(angle));
		}
		else if (angle == 180) {
			x_pos = central_x_pos;
			y_pos = central_y_pos + radius;
		}
		else if (angle < 270) {
			x_pos = central_x_pos - (radius * Math.sin(angle));
			y_pos = central_y_pos + (radius * Math.cos(angle));
		}
		else if (angle == 270) {
			x_pos = central_x_pos;
			y_pos = central_y_pos + radius;
		}
		else {
			x_pos = central_x_pos - (radius * Math.cos(angle));
			y_pos = central_y_pos - (radius * Math.sin(angle));
		}
		
		// Increment the spin angle
		angle = angle + SpinRate;
		if (angle >= 360)
			angle = angle - 360;
		
		// Age the particle and kill it if it has reached the end of its life
		lifespan--;
		if (lifespan<1) {
			expire(p);
			return false;
		}
		else
			return true;
	}
}
