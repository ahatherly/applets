package com.hatherly.Fireworks;

import java.awt.Color;
import java.util.ArrayList;

/**
 * This is the glitter rocket object - it is a child of the
 * RocketParticle object but at set intervals while it is
 * moving it creates "glitter" particles that travel downwards
 * @author Adam Hatherly
 */
public class GlitterRocketParticle extends RocketParticle {

	// Variables to allow for a glitter particle
	private final int GlitterReleaseRate = 1;
	private final int GlitterLifespan = 25;
	private int glitter_delay = 0;
	
	/**
	 * Calls rocket superclass constructor
	 * @param width Display width
	 * @param height Display height
	 */
	public GlitterRocketParticle(int width, int height) {
		super(width, height);
	}
	
	/**
	 * Spawns new particles at set intervals during movement
	 * to simulate "glitter" being released
	 * @param p List of visible particles
	 * @return false if the particle has expired (from superclass)
	 */
	public boolean Move_Particle(ArrayList p)
	{
		double new_h = (Math.random()*0.5)-0.25;
		double new_u = (Math.random()*0.5)-0.5;
		int new_l = (int)(Math.random()*50)+(GlitterLifespan-25);
			
		p.add(new Particle((int)x_pos, (int)y_pos, new_h, new_u, new_l, Color.white));

		glitter_delay++;
		if (glitter_delay == GlitterReleaseRate)
			glitter_delay = 0;
		
		return super.Move_Particle(p);
	}
	
	/**
	 * Decrements the number of rockets on the screen
	 * (this increases the likelihood of another being
	 * launched).
	 */
	public void expire(ArrayList p) {
		RocketLauncher.getInstance().decrement_rockets();
	}
}
