package com.hatherly.Fireworks;

import java.awt.Frame;

public class FireworksApplication {

	public static void main(String[] args) {
		// define frame, its size and make it visible
		Frame myFrame = new FireworkWindow();
		myFrame.setBounds(10, 10, 800, 600); // this time use a predefined frame size/position
		myFrame.setVisible(true);
	}
}
