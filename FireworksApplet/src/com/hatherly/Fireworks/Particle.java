package com.hatherly.Fireworks;

import java.awt.Color;
import java.util.ArrayList;

/**
 * This is the particle object - it models basic particle
 * behaviour and acts as the superclass for more complex
 * types of particles.
 * @author  Adam Hatherly
 */
public class Particle {

	protected final double Gravity = 0.00105;
	protected double horizontal_force;
	protected double upwards_force;
	protected int lifespan;
	protected double x_pos;
	protected double y_pos;
	protected Color colour;
	
	/**
	 * Constructor for all kinds of particles except rockets
	 * @param x X position
	 * @param y Y position
	 * @param h Horizontal force
	 * @param u Upwards Force
	 * @param l Lifespan
	 * @param c Colour
	 */
	public Particle(int x, int y, double h, double u, int l, Color c)
	{
		x_pos = (double)x;
		y_pos = (double)y;
		horizontal_force = h;
		upwards_force = u;
		lifespan = l;
		colour = c;
	}
	
	public Particle() {
	}
	
	/**
	 * Constructor for creating new rockets
	 * @param width Width of display
	 * @param height Height of display
	 */
	public Particle(int height, int width) {
		x_pos = (int)(Math.random()*(width-50)+25);
		y_pos = height;
		if (x_pos > (width/2))
			horizontal_force = Math.random()*-0.2;
		else
			horizontal_force = Math.random()*0.2;
		upwards_force = (Math.random()*0.2)+0.4;
		lifespan = (int)(Math.random()*200)+400;
		colour = Color.white;
	}
	
	/**
	 * Moves the particle, taking gravity and direction
	 * into account.
	 * @param p List of visible particles
	 * @return false if the particle has expired
	 */
	public boolean Move_Particle(ArrayList p)
	{
		x_pos = x_pos + horizontal_force;
		y_pos = y_pos - upwards_force;
		upwards_force = upwards_force - Gravity;
		lifespan--;
		if (lifespan<1) {
			expire(p);
			return false;
		}
		else
			return true;
	}
	
	/**
	 * By default the expire method does nothing
	 * @param p List of visible particles
	 */
	public void expire(ArrayList p) {
	}

}
