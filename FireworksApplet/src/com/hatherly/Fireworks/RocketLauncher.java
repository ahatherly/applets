package com.hatherly.Fireworks;
import java.util.ArrayList;

/**
 * The RocketLauncher is a singleton that deals with creating new Rocket and GlitterRocket objects at random intervals. The probability of launching a rocket is higher when there are less rockets already on the screen.
 * @author     Adam Hatherly
 */
public class RocketLauncher {
	
	private final int MaxRockets = 15;
	private int rocket_count = 0;
	private static RocketLauncher _instance = null;
		
	private RocketLauncher() {
	}
	
	/**
	 * @return Reference to singleton object
	 */
	public static RocketLauncher getInstance() {
		if (_instance == null) {
			synchronized(RocketLauncher.class) {
				if (_instance == null) {
					_instance = new RocketLauncher();
				}
			}
		}
		return _instance;
	}
	
	/**
	 * Randomly launch a rocket, with greater probablility
	 * if there are less already in the air.
	 * @param p List of visible particles
	 * @param width Width of display
	 * @param height Height of display
	 */
	public void launch(ArrayList p, int width, int height)
	{
		int rocket_rand = (int)(Math.random()*(rocket_count+1)*20);
		int glitter_rocket_rand = (int)(Math.random()*(rocket_count+1)*70);

		if (rocket_count<MaxRockets) {

			if (rocket_rand == 0)
			{
				// Launch a Rocket
				increment_rockets();
				p.add(new RocketParticle(height, width));
			}
			if (glitter_rocket_rand == 0)
			{
				// Launch a Glitter Rocket
				increment_rockets();
				p.add(new GlitterRocketParticle(height, width));
			}
			
		}
	}

	/**
	 * Increment count of rockets on the screen
	 */
	public void increment_rockets() {
		rocket_count++;
	}
	
	/**
	 * Decrement count of rockets on the screen
	 */
	public void decrement_rockets() {
		rocket_count--;
	}
}
