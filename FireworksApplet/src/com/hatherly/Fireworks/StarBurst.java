package com.hatherly.Fireworks;
import java.util.ArrayList;
import java.awt.*;

/**
 * This is the StarBurst particle object - it is a child of the
 * particle object but differs in that it does not move, and only
 * lives for a short while. It's purpose is to spawn trails of
 * new particles after a rocket expires.
 * @author Adam Hatherly
 */
public class StarBurst extends Particle {
	
	private final int TailLength = 15;
	private final int TailSpacing = 10;
	double[] h;
	double[] u;
	Color[] c;
	Color[] spectrum;
	int spinning = 0;
	
	/**
	 * @param p List of visible particles
	 * @param x X position
	 * @param y Y position
	 */
	public StarBurst(ArrayList p, int x, int y) {
		super(x,y,0.0,0.0,80,Color.white);
		
		int n = 25;
		spectrum = new Color[ n ];
        for ( int i = 0; i < n; ++i ) {
	         spectrum[i] = new Color( Color.HSBtoRGB(i/(float)n,1,1) );
        }
		
        if ((int)(Math.random()*3) == 0) {
        	spinning =1;
        }
        
		h = new double[50];
	    u = new double[50];
	    c = new Color[50];
		lifespan = TailLength * TailSpacing;
		
		for (n=1; n<10; n++)
		{
			h[n] = (Math.random()*0.5)-0.25;
			u[n] = (Math.random()*0.5)-0.15;
			c[n] = spectrum[(int)(Math.random()*24)];
		}
		new_tail(p);

	}
	
	/**
	 * Adds a new "tail" particle at the starburst's co-ordinates
	 * with random direction of travel.
	 * @param p List of visible particles
	 */
	private void new_tail(ArrayList p) {
		int n;
		int l = (int)(Math.random()*50)+200;
		
		for (n=1; n<10; n++)
		{
			if (spinning == 0)
				p.add(new Particle((int)x_pos, (int)y_pos, h[n], u[n], l, c[n]));
			else
				p.add(new SpinningParticle((int)x_pos, (int)y_pos, h[n], u[n], l, c[n], 3));
		}
	}
	
	/**
	 * The StarBurst does not actually move, instead it creates
	 * a new tail at set periods throughout is't lifespan.
	 * @param p List of visible particles
	 */
	public boolean Move_Particle(ArrayList p) {
		lifespan--;
		if ((lifespan % TailSpacing) == 0)
		{
			new_tail(p);
		}
		if (lifespan<1) {
			expire(p);
			return false;
		}
		else
			return true;
	}
}
