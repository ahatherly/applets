package com.hatherly.Fireworks;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FireworkWindow extends Frame implements ActionListener {
	
	 public FireworkWindow() {
		super("Fireworks");
		MenuBar mb = new MenuBar();
		setMenuBar(mb);
		// Define Exit menu item
		Menu fileMenu = new Menu("File");
		mb.add(fileMenu);
		MenuItem exitMenuItem = new MenuItem("Exit");
		fileMenu.add(exitMenuItem);
		exitMenuItem.addActionListener (this);
		// define the applet and add to the frame
		Fireworks fireworksApplet = new Fireworks(); // define applet of interest
		setSize(800, 600);
		fireworksApplet.init();
		// add applet to the frame
		add(fireworksApplet, BorderLayout.CENTER);
		setVisible(true); // make frame visible
		fireworksApplet.start();
	 }
	 
	 public void actionPerformed(ActionEvent evt) {
	    if (evt.getSource() instanceof MenuItem) {
			String menuLabel = ((MenuItem)evt.getSource()).getLabel();
			if(menuLabel.equals("Exit")) {
			    // close application when exit is selected
			    dispose();
			    System.exit(0);
			}
	    }
	 }
}
