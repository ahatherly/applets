package com.hatherly.Fireworks;

import java.awt.Color;
import java.util.ArrayList;

/**
 * This is the rocket particle object - it is a child of the particle
 * object but when it expires it creates a new StarBurst object.
 * @author Adam Hatherly
 */
public class RocketParticle extends Particle {

	/**
	 * Constructor for creating new rockets
	 * @param width Width of display
	 * @param height Height of display
	 */
	public RocketParticle(int height, int width) {
		double h = height;
		x_pos = (int)(Math.random()*(width-50)+25);
		y_pos = height;
		if (x_pos > (width/2))
			horizontal_force = Math.random()*-0.3;
		else
			horizontal_force = Math.random()*0.3;
		//upwards_force = (Math.random()*0.2)+0.4;
		upwards_force = (Math.random()*0.2)+0.2+(h/1000);
		lifespan = (int)(Math.random()*200)+height+200;
		colour = Color.white;
	}
	
	/**
	 * Calls superclass to move particle
	 * @param p List of visible particles
	 * @return false if the particle has expired (from superclass)
	 */
	public boolean Move_Particle(ArrayList p)
	{
		return super.Move_Particle(p);
	}
	
	/**
	 * When the rocket expires, a new StarBurst is created
	 * at the rocket's position. Also decrements the number
	 * of rockets on the screen (this increases the
	 * likelihood of another being launched).
	 * @param p List of visible particles
	 */
	public void expire(ArrayList p) {
		// Create a new StarBurst
		p.add(new StarBurst(p, (int)x_pos, (int)y_pos));
		RocketLauncher.getInstance().decrement_rockets();
	}
}
